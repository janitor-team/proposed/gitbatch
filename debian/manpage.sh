#!/bin/bash

help2man gitbatch --no-discard-stderr --version-string="$(dpkg-parsechangelog -S Version | cut -d- -f1)" -n "Manage git repositories in one place" > debian/gitbatch.1
